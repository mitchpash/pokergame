﻿namespace PokerGame
{
    public class Card
    {
        public enum Rank
        {
            Two = 2, 
            Three,
            Four,
            Five,
            Six,
            Seven,
            Eight,
            Nine,
            Ten,
            Jack,
            Queen,
            King,
            Ace
        }
        public enum Suit
        {
            Heart = 1,
            Diamond,
            Club,
            Spade
        }

        public Rank CardRank { get; set; }
        public Suit CardSuit { get; set; }

        public Card(int suit, int rank) //start constructor
        {
            this.CardRank = (Rank) rank;
            this.CardSuit = (Suit) suit;
        } //end constructor 

        public override string ToString()
        {
            return CardRank + " of " + CardSuit + "s";
        }
    }
}