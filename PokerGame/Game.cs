﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerGame
{
    class Game
    {
        static void Main(string[] args)
        {
            //Make a deck of 52 cards
            var deck = new Deck();
            //Courtesy Shuffle
            //deck.Shuffle();
            //Print out each card on a new line
            for (int i = 0; i < 52; i++)
            {
                Console.WriteLine(deck.ListOfCards[i]);
            }

            //End of Program
            Console.ReadKey();
        }
    }
}
